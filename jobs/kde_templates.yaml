# yaml templates for kde packages
# Copyright © 2016 Maximiliano Curia <maxy@gnuservers.com.ar>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

- job-group:
    name: 'frameworks-{item}'
    arch:
        - amd64
        - i386
        - armhf
    jobs:
        - '{item}_prepare'
        - '{item}_build'
        - '{item}_test':
            dependencies: '{obj:test_dependencies}'
            arch: amd64
    dependencies: '{obj:build_dependencies}'
    reverse_build_dependencies: '{obj:reverse_build_dependencies}'
    description: '{description}'
    remotes: '{obj:remotes}'

- job-group:
    name: 'plasma-{item}'
    arch:
        - amd64
        - i386
        - armhf
    jobs:
        - '{item}_prepare'
        - '{item}_build'
        - '{item}_test':
            dependencies: '{obj:test_dependencies}'
            arch: amd64
    dependencies: '{obj:build_dependencies}'
    reverse_build_dependencies: '{obj:reverse_build_dependencies}'
    description: '{description}'
    remotes: '{obj:remotes}'

- job-group:
    name: 'applications-{item}'
    arch:
        - amd64
        - i386
        - armhf
    jobs:
        - '{item}_prepare'
        - '{item}_build'
        - '{item}_test':
            dependencies: '{obj:test_dependencies}'
            arch: amd64
    dependencies: '{obj:build_dependencies}'
    reverse_build_dependencies: '{obj:reverse_build_dependencies}'
    description: '{description}'
    remotes: '{obj:remotes}'

- job-group:
    name: 'extras-{item}'
    arch:
        - amd64
        - i386
        - armhf
    jobs:
        - '{item}_prepare'
        - '{item}_build'
        - '{item}_test':
            dependencies: '{obj:test_dependencies}'
            arch: amd64
    dependencies: '{obj:build_dependencies}'
    reverse_build_dependencies: '{obj:reverse_build_dependencies}'
    description: '{description}'
    remotes: '{obj:remotes}'

- job-group:
    name: 'req-{item}'
    arch:
        - amd64
        - i386
        - armhf
    jobs:
        - '{item}_prepare'
        - '{item}_build'
        - '{item}_test':
            dependencies: '{obj:test_dependencies}'
            arch: amd64
    dependencies: '{obj:build_dependencies}'
    reverse_build_dependencies: '{obj:reverse_build_dependencies}'
    description: '{description}'
    remotes: '{obj:remotes}'

- job-group:
    name: 'std-{item}'
    arch:
        - amd64
        - i386
        - armhf
    jobs:
        - '{item}_prepare'
        - '{item}_build'
        - '{item}_test':
            dependencies: '{obj:test_dependencies}'
            arch: amd64
    dependencies: '{obj:build_dependencies}'
    reverse_build_dependencies: '{obj:reverse_build_dependencies}'
    description: '{description}'
    remotes: '{obj:remotes}'

- defaults:
    name: global
    quiet-period: 5
    block-downstream: false
    block-upstream: false
    retry-count: 3
    logrotate:
        daysToKeep: 64

- job-template:
    name: '{item}_prepare'
    description: '{description} -- Prepare'
    node: prepare
    parameters:
        - string:
            name: DISTRIBUTION
            default: unreleased
            description: 'Target distribution'
    scm:
        - git:
            basedir: 'repo'
            remotes: '{obj:remotes}'
            local-branch: master
            wipe-workspace: false
            skip-tag: true
            prune: true
            # This should be a configuration parameter, somehow
            excluded-users:
                - 'Automatic packaging'
            branches:
                - debian/master
                - local/master
    triggers:
        - pollscm:
            cron: 'H * * * *'
        - script:
            script: '/srv/pkg-kde-jenkins/scripts/prepare_trigger.sh'
            cron: 'H H/6 * * *'
    builders:
        - shell: '/srv/pkg-kde-jenkins/scripts/clean_build.sh'
        # Needs the Copy Artifacts plugin
        - copyartifact:
            project: '{item}_build'
            filter: 'build/*.build'
            which-build: last-completed
            parameter-filters: 'DISTRIBUTION=${{DISTRIBUTION}}'
            optional: true
        - shell: '/srv/pkg-kde-jenkins/scripts/prepare_build.sh'
    publishers:
        - conditional-publisher:
            - condition-kind: file-exists
              condition-filename: 'build/trigger_build'
              action:
                - trigger-parameterized-builds:
                    - project: '{item}_build'
                      current-parameters: true
                      condition: UNSTABLE_OR_BETTER
        - archive:
            artifacts: 'build/'
            allow-empty: true

- job-template:
    name: '{item}_build'
    # Needs the matrix project plugin
    project-type: matrix
    # Only affects the lightweight job
    node: master
    parameters:
        - string:
            name: DISTRIBUTION
            default: unreleased
            description: 'Target distribution'
    description: '{description} -- Build'
    block-upstream: true
    execution-strategy:
        touchstone:
            expr: 'arch == "amd64"'
            result: unstable
    axes:
        - axis:
            type: slave
            name: arch
            values:
                - amd64
                - armhf
                - i386
    scm: []
    builders:
        - shell: '/srv/pkg-kde-jenkins/scripts/clean_build.sh'
        # Needs the Copy Artifacts plugin
        - copyartifact:
            project: '{item}_prepare'
            filter: 'build/'
            which-build: upstream-build
            fallback-to-last-successful: true
        - shell: '/srv/pkg-kde-jenkins/scripts/build_build.sh'
    publishers:
        - trigger-parameterized-builds:
            - project: '{item}_test'
              current-parameters: true
              condition: UNSTABLE_OR_BETTER
        - archive:
            artifacts: 'build/'
            allow-empty: true
        # Needs the Flexible publisher plugin
        - conditional-publisher:
            - condition-kind: never
              action:
                 - trigger:
                     project: '{reverse_build_dependencies}'
                     condition: UNSTABLE

- job-template:
    name: '{item}_test'
    description: '{description} -- Test'
    node: lxc_amd64
    parameters:
        - string:
            name: DISTRIBUTION
            default: unreleased
            description: 'Target distribution'
    scm: []
    builders:
        - shell: '/srv/pkg-kde-jenkins/scripts/clean_build.sh'
        - copyartifact:
            project: '{item}_build/arch=amd64'
            filter: 'build/'
            which-build: upstream-build
            fallback-to-last-successful: true
        - shell: '/srv/pkg-kde-jenkins/scripts/test_build.sh'
    publishers:
        - archive:
            artifacts: 'build/'
            allow-empty: true
        - junit:
            results: 'build/*.xml'
